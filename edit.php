<?php
include("db.php");

if(isset($_GET['id'])){
    $id = $_GET['id'];

    $sql = "SELECT * FROM tasks WHERE id=$id";
    $result = $conn->query($sql);
    $row = $result->fetch_assoc();
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Edit Task</title>
</head>
<body>
    <h2>Edit Task</h2>

    <form action="tasks.php" method="POST">
        <input type="hidden" name="id" value="<?php echo $row['id']; ?>">

        <label for="task_name">Task Name:</label>
        <input type="text" name="task_name" value="<?php echo $row['task_name']; ?>" required>

        <label for="task_description">Task Description:</label>
        <textarea name="task_description"><?php echo $row['task_description']; ?></textarea>

        <button type="submit" name="update">Update Task</button>
    </form>
</body>
</html>
