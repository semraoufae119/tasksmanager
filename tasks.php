<?php
include("db.php");

// Create Task
if(isset($_POST['create'])){
    $task_name = $_POST['task_name'];
    $task_description = $_POST['task_description'];

    $sql = "INSERT INTO tasks (task_name, task_description) VALUES ('$task_name', '$task_description')";
    $conn->query($sql);

    header("Location: /");
}

// Read Tasks
$sql = "SELECT * FROM tasks";
$result = $conn->query($sql);

// Update Task
if(isset($_POST['update'])){
    $id = $_POST['id'];
    $task_name = $_POST['task_name'];
    $task_description = $_POST['task_description'];

    $sql = "UPDATE tasks SET task_name='$task_name', task_description='$task_description' WHERE id=$id";
    $conn->query($sql);

    header("Location: /");
}

// Delete Task
if(isset($_GET['delete'])){
    $id = $_GET['delete'];

    $sql = "DELETE FROM tasks WHERE id=$id";
    $conn->query($sql);

    header("Location: /");
}
