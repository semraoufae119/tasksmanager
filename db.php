<?php

// Connect to DB
$host = 'localhost'; // localhost
$user = 'root'; 
$password = ''; // password
$database = 'todolist';

$conn = new mysqli($host, $user, $password, $database); // PDO

if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}